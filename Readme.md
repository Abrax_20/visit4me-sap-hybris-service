# Visit4me SAP Service
This project is a microservice for the Visit4me UG to handle Hybris SAP login data and api requests.
### Mock
In this Project is a short mock of the Visit4me main api intigrated with following routes
- GET /accounts/ to check if the user is authenticated you need to send a x-access-token to this endpoint (This token is in the request header of the user (x-access-token: token))
- POST /auth/login to get a access token for a example user
### Todos
- Intigrate a credatial manager with mongodb and save a referenze to the user (Please check the /account route)
- Add a User to SAP CRM route
- Add find User route
### Notes
Use following links:
- https://github.com/SAP/C4CODATAAPIDEVGUIDE/
- https://www.npmjs.com/package/odata
- https://www.npmjs.com/package/xml2json

Example code to add a Contact (Please use the fetch api)
```
var http = require("https");

var options = {
  "method": "POST",
  "hostname": "my334079.crm.ondemand.com",
  "port": null,
  "path": "/sap/c4c/odata/v1/c4codataapi/LeadCollection/",
  "headers": {
    "authorization": "Basic SAP_TOKEN",
    "content-length": "48",
    "x-csrf-token": SAP_CSRF_TOKE,
    "content-type": "application/json"
  }
};

var req = http.request(options, function (res) {
  var chunks = [];

  res.on("data", function (chunk) {
    chunks.push(chunk);
  });

  res.on("end", function () {
    var body = Buffer.concat(chunks);
    console.log(body.toString());
  });
});

req.write(JSON.stringify({ Company: 'Visit4me', Name: 'Christoph' }));
req.end();
```

Please ask the Author for more questions

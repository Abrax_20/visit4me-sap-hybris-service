require('dotenv').config();
const cors = require('cors');
const morgan = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');

const routes = require('./routes');

const app = express();
const port = process.env.PORT || 3000;


app.use(cors());
app.set('trust proxy', 1);
app.use(
  session({
    proxy: true,
    secret: process.env.SESSION || 'S3CRE7',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true },
    key: 'outlook_session.sid',
  })
);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(routes);

app.get('/', function (req, res) {
  res.send('Office API is Running!');
});

app.listen(port, () => {
  console.log('Outlook service started on port ' + port);
});

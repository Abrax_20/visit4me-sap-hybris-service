const fetch = require('node-fetch');

const getBase64AuthToken = (username, password) => {
  const buffer = Buffer.from(`${username}:${password}`);
  return buffer.toString('base64');
};

const handleLogin = async ({
                       username,
                       password,
                       instants,
                     }) => {
  try {
    const token = getBase64AuthToken(username, password);
    console.log(token);
    const res = await fetch(`https://${instants}/sap/c4c/odata/v1/c4codataapi/`, {
      method: 'GET',
      headers: {
        Authorization: `Basic ${token}`
      }
    });
    console.log(res);
  } catch (e) {
    console.log(e);
  }
};

module.exports = {
  handleLogin
};

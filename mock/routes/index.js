const { Router } = require('express');

const auth = require('./auth');
const account = require('./account');

const index = Router();

index.use('/auth', auth);
index.use('/account', account);

module.exports = index;

const { Router } = require('express');

const account = Router();

account.get('/', (_, res) => res.json({
  "id": "5c422d6706819300110f9202",
  "success": true,
  "profileType": 0,
  "activated": false,
  "username": "moritz.tittler@visit4.me",
  "profilePicture": null,
  "profileHeaderImage": null,
  "firstname": "Moritz",
  "lastname": "Tittler",
  "company": {
    "id": "5c422d6706819300110f9201",
    "title": "Visit4me"
  },
  "description": "",
  "salesforce_account": null,
  "services": [
    {
      "id": "5c422d6706819300110f9203",
      "type": "email",
      "data": {
        "type": "Email",
        "email": "moritz.tittler@visit4.me"
      }
    }
  ],
  "email": "moritz.tittler@visit4.me"
}));

module.exports = account;

const { handleLogin } = require('../../../helper/auth');

const login = async (req, res) => {
  const {
    username,
    password,
    instants,
  } = req.body || {};

  console.log(username, password, instants);

  handleLogin({username, password, instants})

  res.json({});
};
module.exports = login;

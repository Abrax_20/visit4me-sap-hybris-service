const { Router } = require('express');

const login = require('./post/login');

const auth = Router();

auth.post('/', login);

module.exports = auth;

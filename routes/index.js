const { Router } = require('express');

const auth = require('./auth');

const index = Router();

index.use('/auth', auth);

module.exports = index;
